package com.devcamp.s50.customer_visit.visitapi.model;

public class Customer {
    String name;
    boolean member = false;
    String memberType;
    
    public Customer(String name) {
        this.name = name;
    }
    
    public Customer(String name, boolean member, String memberType) {
        this.name = name;
        this.member = member;
        this.memberType = memberType;
    }

    public String getName() {
        return name;
    }
    public boolean isMember() {
       return true;
    }
    public void setMember(boolean member) {
        this.member = member;
    }
    public String getMemberType() {
        return memberType;
    }
    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }
    public String toString() {
        return "Customer[name= " + name + "member type= " + memberType + "]";
    }

}
