package com.devcamp.s50.customer_visit.visitapi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.customer_visit.visitapi.model.Customer;
import com.devcamp.s50.customer_visit.visitapi.model.Visit;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("visits")
    public ArrayList<Visit> getVisitList() throws ParseException {
        //tạo 3 đối tượng customer
        Customer customer1 = new Customer("OHMM", true, "GOLD");
        Customer customer2 = new Customer("OHMM", true, "GOLD");
        Customer customer3 = new Customer("OHMM", true, "GOLD");
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        System.out.println(customer3.toString());

        //tạo 3 đối tượng visit
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = format.parse("2022-07-15");
        Date date2 = format.parse("2022-06-15");
        Date date3 = format.parse("2022-05-15");

        Visit visit1 = new Visit(customer1, date1);
        Visit visit2 = new Visit(customer2, date2);
        Visit visit3 = new Visit(customer3, date3);

        //tạo arrayList visit và add visit vào
        ArrayList<Visit> visitList = new ArrayList<Visit>();
        visitList.add(visit1);
        visitList.add(visit2);
        visitList.add(visit3);
        return visitList;
    }
}
