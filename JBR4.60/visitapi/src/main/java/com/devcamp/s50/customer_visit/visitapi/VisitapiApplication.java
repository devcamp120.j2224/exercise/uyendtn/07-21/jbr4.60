package com.devcamp.s50.customer_visit.visitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisitapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VisitapiApplication.class, args);
	}

}
