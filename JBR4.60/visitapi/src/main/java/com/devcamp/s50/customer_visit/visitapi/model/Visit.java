package com.devcamp.s50.customer_visit.visitapi.model;

import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense = 500000;
    double productExpense = 200000;

    

    public Visit() {
    }
    
    
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }


    public Customer getCustomer() {
        return customer;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpense() {
        return this.getServiceExpense() + this.getProductExpense();
    }

    public String toString() {
        return "Visit[customer = " + customer.getName() + ", date = " + date + ", total expense= " + this.getTotalExpense() + "]";
    }
    
}
